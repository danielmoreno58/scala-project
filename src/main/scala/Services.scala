package copernikus

trait TelescopioInterface:
  def abrirParasol(telescopio:Telescopio, temperatura: Int): Telescopio
  def inicializarBus(telescopio: Telescopio): Unit

trait TelescopioService extends TelescopioInterface:
  def abrirParasol(telescopio: Telescopio, temperatura: Int): Telescopio = {
    println(temperatura)

    if (temperatura > 40){
      val parasolDesplegado = Parasol (
        "Desplegado"
      )
      val telescopio1 = telescopio.copy(parasol = parasolDesplegado)
      telescopio1
    }else{
      telescopio
    }
  }

  def inicializarBus(telescopio: Telescopio): Unit = {
    if (telescopio.parasol.status == "Desplegado")
      Bus.inicialization
  }

end TelescopioService
