package copernikus


@main def telescopioServiceMain = {
  object TelescopioService extends TelescopioService
  import TelescopioService.*

  // Abrir Parasol
  val parasol = Parasol("SinDesplegar")

  val telescopio = Telescopio(
    parasol: Parasol,
    0
  )

  val telescopioResultado = TelescopioService.abrirParasol(telescopio, 50)
  println(telescopioResultado.parasol.status)

  //Inicializar Bus
  TelescopioService.inicializarBus(telescopioResultado)

  val espejo: Espejo = Espejo(Seq())
  val espejo1 = Telescopio.MontajeEspejo(espejo)
  val espejo2 = Telescopio.alineacionEspejo(espejo1, 90)

}

