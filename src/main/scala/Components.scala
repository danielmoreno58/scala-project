package copernikus

val randomutil = scala.util.Random

enum estadoParasol:
  case SinDesplegar, Desplegado

case class Telescopio(
 parasol: Parasol,
 temperatura: Int
)

case class modulo(nombre: String, estado: String)

object Bus {
  val modulos: List[modulo] = List(modulo("computacion", "apagado"),
                                   modulo("computacion", "apagado"),
                                   modulo("propulcion", "apagado"),
                                   modulo("memoria", "apagado"),
                                   modulo("posicionamiento", "apagado"),
                                   modulo("IA", "apagado"))

  def inicialization: Unit = {
    modulos.map(modulo => {
      println(s"inicializando ${modulo.nombre}...")
      val moduloNuevo = modulo.copy(modulo.nombre, "encendido")
      println(s"modulo ${moduloNuevo.estado}")
      moduloNuevo
    })
  }

}

// companion object
object Telescopio {
  def MontajeEspejo(espejo: Espejo): Espejo = {
    var segmentoEspejo = Seq[Segmento]()
    for (numero <- 1 to 18) {
      segmentoEspejo = segmentoEspejo :+ Segmento(numero, "Inactivo", Actuador(randomutil.nextInt(180)))
    }
    espejo.copy(segmentoEspejo)
  }

  def alineacionEspejo(espejo: Espejo, gradosInclinacion: Int): Espejo = {
    var segmentoEspejo = Seq[Segmento]()
    if (espejo.segmentos.size == 18){
      for (numero <- 1 to 18) {
        segmentoEspejo = segmentoEspejo :+ Segmento(numero, "Activo", Actuador(gradosInclinacion))
        println(s"Espejo ${segmentoEspejo(numero-1).numero} ${segmentoEspejo(numero-1).status} con ${segmentoEspejo(numero-1).actuador.gradosInclinacion} grados de inclinacion")
      }
    }
    espejo
  }

}

case class Parasol(
  status: String
)

case class Espejo(
  segmentos: Seq[Segmento]
)

case class Segmento(
  numero: Int,
  status: String,
  actuador: Actuador
)

case class Actuador(
   gradosInclinacion: Int
)

case class Sensor(
  foto: String
)